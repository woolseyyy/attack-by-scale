"""
simple_script.py
An simple script to roughly verify possible methods of scale attacking
To speed up verifying process, only attack on one or several pictures instead of the whole test set

2018/08/28 woolsey@zju.edu.cn
"""

import torch
import pretrainedmodels
import pretrainedmodels.utils as utils
import argparse
import numpy as np
from PIL import Image
from seam_carving import SeamCarver
import os


def resize(src, new_size):
    """
    resize numpy array to specific size using 'bilinear', designed for jacobian map
    :param src: src numpy, 3 x W x H
    :param new_size: (W, H)
    :return: resized array
    """
    dst_w, dst_h = new_size
    src_h, src_w = src.shape[1:]
    if src_h == dst_h and src_w == dst_w:
        return src.copy()
    scale_x = float(src_w) / dst_w
    scale_y = float(src_h) / dst_h

    dst = np.zeros((3, dst_h, dst_w), dtype=src.dtype)
    for n in range(3):  # iteration in chanel
        for dst_y in range(dst_h):  # iteration in height
            for dst_x in range(dst_w):  # iteration in width
                # position on source map
                src_x = (dst_x + 0.5) * scale_x - 0.5
                src_y = (dst_y + 0.5) * scale_y - 0.5
                # neighbour on source map
                src_x_0 = int(np.floor(src_x))
                src_y_0 = int(np.floor(src_y))
                src_x_1 = min(src_x_0 + 1, src_w - 1)
                src_y_1 = min(src_y_0 + 1, src_h - 1)

                # calculate value in linear
                value0 = (src_x_1 - src_x) * src[n, src_y_0, src_x_0] + (src_x - src_x_0) * src[n, src_y_0, src_x_1]
                value1 = (src_x_1 - src_x) * src[n, src_y_1, src_x_0] + (src_x - src_x_0) * src[n, src_y_1, src_x_1]
                dst[n, dst_y, dst_x] = (src_y_1 - src_y) * value0 + (src_y - src_y_0) * value1
    return dst


def scale_attack_1(image, model):
    """
    Produce a scaled image of input image to mislead the model
    Method 1: delete max jacobian seam of current class
    :param image: PIL image, which scales the pixel values between 0 and 1
    :param model: trained model object, should have api: model()
    :return: attacked image, PIL image
    """

    # get Jacobian map
    jacobian_map = get_jacobian_map(image, model, None)
    weight_mask = np.abs(jacobian_map.sum(axis=0)) * -1  # seam carving pick mini weight as result seam

    # do seam carving
    seamCarver = SeamCarver(image, image.size[0]-100, image.size[1]-100, weight_mask, weight_after_seam=True, weight_constant=1000)
    attacked = seamCarver.start()

    # return result
    return attacked, weight_mask * -1


def scale_attack_2(image, model):
    """
    Produce a scaled image of input image to mislead the model
    Method 2: add max jacobian seam of other class
    :param image: PIL image, which scales the pixel values between 0 and 1
    :param model: trained model object, should have api: model()
    :return: attacked image, PIL image
    """

    # get Jacobian map
    jacobian_map = get_jacobian_map(image, model, 1)
    weight_mask = np.abs(jacobian_map.sum(axis=0)) * -1  # seam carving pick mini weight as result seam

    # do seam carving
    seamCarver = SeamCarver(image, image.size[0]+50, image.size[1]+50, weight_mask, weight_after_seam=True, weight_constant=1000)
    attacked = seamCarver.start()

    # return result
    return attacked, weight_mask * -1


def scale_attack_3(image, model):
    """
    Produce a scaled image of input image to mislead the model
    Method 3: add min jacobian seam of current class and delete max jacobian seam of current class
    :param image: PIL image, which scales the pixel values between 0 and 1
    :param model: trained model object, should have api: model()
    :return: attacked image, PIL image
    """

    for i in range(1):
        # delete max weight pixel
        # get Jacobian map
        jacobian_map = get_jacobian_map(image, model, None)
        weight_mask = np.abs(jacobian_map.sum(axis=0)) * -1  # seam carving pick mini weight as result seam

        # do seam carving - delete mode
        seamCarver = SeamCarver(image, image.size[0] - 50, image.size[1] - 50, weight_mask, weight_after_seam=True,
                                weight_constant=1000)
        attacked = seamCarver.start()
        weight_mask = seamCarver.get_weight_mask()

        image = attacked

        # add min weight pixel
        # do seam carving - add mode
        seamCarver = SeamCarver(image, image.size[0] + 50, image.size[1] + 50, weight_mask, weight_after_seam=True,
                                weight_constant=1000)
        attacked = seamCarver.start()
        weight_mask = seamCarver.get_weight_mask()

        image = attacked

    # return result
    return attacked, weight_mask

def scale_attack_4(image, model):
    """
    Produce a scaled image of input image to mislead the model
    Method 3: add min jacobian seam of current class and delete max jacobian seam of current class
    :param image: PIL image, which scales the pixel values between 0 and 1
    :param model: trained model object, should have api: model()
    :return: attacked image, PIL image
    """

    for i in range(50):
        # delete max weight pixel
        # get Jacobian map
        jacobian_map = get_jacobian_map(image, model, None)
        weight_mask = np.abs(jacobian_map.sum(axis=0)) * -1  # seam carving pick mini weight as result seam

        # do seam carving - delete mode
        seamCarver = SeamCarver(image, image.size[0] - 10, image.size[1] - 10, weight_mask, weight_after_seam=False,
                                weight_constant=1000)
        attacked = seamCarver.start()
        weight_mask = seamCarver.get_weight_mask()

        image = attacked

        # add min weight pixel
        # do seam carving - add mode
        seamCarver = SeamCarver(image, image.size[0] + 10, image.size[1] + 10, weight_mask, weight_after_seam=False,
                                weight_constant=1000)
        attacked = seamCarver.start()
        weight_mask = seamCarver.get_weight_mask()

        image = attacked

    # return result
    return attacked, weight_mask


def get_jacobian_map(image, model, target=None):
    """
    compute Jacobian map of model of some target.
    :param image: PIL image
    :param model: trained model object
    :param target: target class, if it is none, the function will take the max possibility class
    :return: jacobian map of image, numpy array format
    """

    tf_img = utils.TransformImage(model)
    input_tensor = tf_img(image)
    input_tensor = input_tensor.unsqueeze(0)  # CWH --> NCWH
    input_tensor.requires_grad_()

    model.eval()
    model.zero_grad()
    output_logits = model(input_tensor)  # 1 x class_number tensor

    if target is None:  # take the max possibility class
        _, max_class_index_tensor = output_logits.max(1)
        target = max_class_index_tensor[0]

    class_logit = output_logits[0, target]
    class_logit.backward()
    jacobian_tensor = input_tensor.grad  # NCWH
    jacobian_map = jacobian_tensor[0].numpy()

    # resize
    jacobian_map_resized = resize(jacobian_map, image.size)

    return jacobian_map_resized


def get_predict(image, model, target=None):
    """
    Get model predict result of image
    :param image: PIL image
    :param model: trained model object
    :param target: target class want to watch
    :return: (class_index, score)
    """
    tf_img = utils.TransformImage(model)
    input_tensor = tf_img(image)
    input_tensor = input_tensor.unsqueeze(0)  # CWH --> NCWH
    # input_tensor.requires_grad_()

    model.eval()

    output_logits = model(input_tensor)  # 1 x class_number tensor

    if target is None:  # take the max possibility class
        _, max_class_index_tensor = output_logits.max(1)
        target = max_class_index_tensor[0]

    class_logit = output_logits[0, target]

    return target.item(), id_to_name(target.item()), class_logit.item()


def id_to_name(class_id):
    """
    Convert output logits index to class name
    :param class_id: class id in output logits
    :return: class name, string
    """
    # load Imagenet synsets
    with open('data/imagenet_synsets.txt', 'r') as f:
        synsets = f.readlines()

    synsets = [x.strip() for x in synsets]
    splits = [line.split(' ') for line in synsets]
    key_to_classname = {spl[0]: ' '.join(spl[1:]) for spl in splits}

    # load Imagenet classes
    with open('data/imagenet_classes.txt', 'r') as f:
        class_id_to_key = f.readlines()

    class_id_to_key = [x.strip() for x in class_id_to_key]

    # convert
    class_key = class_id_to_key[class_id]
    classname = key_to_classname[class_key]

    return classname


def array_to_image(array):
    min = array.min()
    delta = array.max() - array.min()

    array = (array - min) / (delta if delta != 0 else 1)  # convert to 0-1
    array = array * 255  # convert to 0-255

    return Image.fromarray(array, 'P').convert('RGB')


def array_to_image_rgb(array):
    min = array.min()
    delta = array.max() - array.min()

    array = (array - min) / (delta if delta != 0 else 1)  # convert to 0-1
    array = array * 255  # convert to 0-255

    return Image.fromarray(np.uint8(array))


def crop_image(image, model):
    tf_img = utils.TransformImage(model)
    input_tensor = tf_img(image)
    image = array_to_image_rgb(input_tensor.transpose(0, 2).transpose(0, 1).data.numpy())
    return image


def main(args):

    # read in image
    load_img = utils.LoadImage()
    image = load_img(args.image)  # CWH 0-255

    # model init
    model = pretrainedmodels.__dict__[args.model_name](num_classes=1000, pretrained='imagenet')

    # crop image
    image = crop_image(image, model)

    # attack
    attacked, weight = scale_attack_3(image, model)

    # test result
    old = get_predict(image, model)
    new = get_predict(attacked, model)

    # show result
    print("Original: {}/{}".format(old[1], old[2]))
    print("Attacked: {}/{}".format(new[1], new[2]))

    image.show()
    attacked.show()
    array_to_image(weight).show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Params')
    parser.add_argument('--model_name', nargs='?', type=str, default='resnet50',
                        help='Model to attack [\'resnet50\']')
    parser.add_argument('--image', nargs='?', type=str, required=True,
                        help='Image path to attack')
    args = parser.parse_args()
    main(args)













