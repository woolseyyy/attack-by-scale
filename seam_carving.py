import numpy as np
import cv2
from PIL import Image

class SeamCarver:
    def __init__(self,  in_image, out_width, out_height,
                 weight_mask='', weight_after_seam=False, weight_constant=0):
        """
        Seam carver init
        :param in_image:  input image, PIL image
        :param out_width: destination width
        :param out_height: destination height
        :param weight_mask: weight mask to pick seam, WxH
        :param weight_after_seam: if use weight map after seam producing
        :param weight_constant: constant when using weight map, float
        """
        # initialize parameter
        self.out_height = out_height
        self.out_width = out_width

        # read in image and store as np.float64 format
        self.in_image = np.asarray(in_image, dtype=np.float64)
        self.in_height, self.in_width = self.in_image.shape[: 2]

        # keep tracking resulting image
        self.out_image = np.copy(self.in_image)

        # weight_mask to weight seam
        # if weight mask is '' make it zeros
        self.weight_mask = weight_mask if weight_mask != '' else np.zeros((self.in_width, self.in_height))
        self.weight_after_seam = weight_after_seam  # use weight mask during seam finding or after seam finding
        self.weight_constant = weight_constant  # constant of using weight mask

    def start(self):
        """
        :return:
        seam carving function (image retargeting) will be process
        """
        self.seams_carving()

        return self.get_result()

    def seams_carving(self):
        """
        :return:

        We first process seam insertion or removal in vertical direction then followed by horizontal direction.

        If targeting height or width is greater than original ones --> seam insertion,
        else --> seam removal

        The algorithm is written for seam processing in vertical direction (column), so image is rotated 90 degree
        counter-clockwise for seam processing in horizontal direction (row)
        """

        # calculate number of rows and columns needed to be inserted or removed
        delta_row, delta_col = int(self.out_height - self.in_height), int(self.out_width - self.in_width)

        # remove column
        if delta_col < 0:
            self.seams_removal(delta_col * -1)
        # insert column
        elif delta_col > 0:
            self.seams_insertion(delta_col)

        # remove row
        if delta_row < 0:
            self.out_image = self.rotate_image(self.out_image, 1)
            self.weight_mask = self.rotate_weight(self.weight_mask, 1)
            self.seams_removal(delta_row * -1)
            self.out_image = self.rotate_image(self.out_image, 0)
            self.weight_mask = self.rotate_weight(self.weight_mask, 0)
        # insert row
        elif delta_row > 0:
            self.out_image = self.rotate_image(self.out_image, 1)
            self.weight_mask = self.rotate_weight(self.weight_mask, 1)
            self.seams_insertion(delta_row)
            self.out_image = self.rotate_image(self.out_image, 0)
            self.weight_mask = self.rotate_weight(self.weight_mask, 0)

    def seams_removal(self, num_pixel):
        for dummy in range(num_pixel):
            energy_map = self.calc_energy_map()
            weight_energy_map = self.calc_weight_energy_map(energy_map)
            cumulative_map = self.calc_cumulative_map(weight_energy_map)
            seam_idx = self.find_seam(cumulative_map)
            self.delete_seam(seam_idx)
            self.delete_seam_on_weight(seam_idx)

    def seams_insertion(self, num_pixel):
        temp_image = np.copy(self.out_image)
        temp_mask = np.copy(self.weight_mask)
        seams_record = []

        for dummy in range(num_pixel):
            energy_map = self.calc_energy_map()
            weight_energy_map = self.calc_weight_energy_map(energy_map)
            cumulative_map = self.calc_cumulative_map(weight_energy_map)
            seam_idx = self.find_seam(cumulative_map)
            seams_record.append(seam_idx)
            self.delete_seam(seam_idx)
            self.delete_seam_on_weight(seam_idx)

        self.out_image = np.copy(temp_image)
        self.weight_mask = np.copy(temp_mask)
        n = len(seams_record)
        for dummy in range(n):
            seam = seams_record.pop(0)
            self.add_seam(seam)
            self.add_seam_on_weight(seam)
            seams_record = self.update_seams(seams_record, seam)

    def calc_energy_map(self):
        b, g, r = cv2.split(self.out_image)
        b_energy = np.absolute(cv2.Scharr(b, -1, 1, 0)) + np.absolute(cv2.Scharr(b, -1, 0, 1))
        g_energy = np.absolute(cv2.Scharr(g, -1, 1, 0)) + np.absolute(cv2.Scharr(g, -1, 0, 1))
        r_energy = np.absolute(cv2.Scharr(r, -1, 1, 0)) + np.absolute(cv2.Scharr(r, -1, 0, 1))

        return b_energy + g_energy + r_energy

    def calc_weight_energy_map(self, energy_map):
        weight_map = self.weight_mask

        # normalize
        energy_map_l2 = np.linalg.norm(energy_map)
        weight_map_l2 = np.linalg.norm(weight_map)
        energy_map = energy_map / (energy_map_l2 if energy_map_l2 != 0 else 1)
        weight_map = weight_map / (weight_map_l2 if weight_map_l2 != 0 else 1)

        weight_constant = 0 if self.weight_after_seam else self.weight_constant

        return energy_map + weight_constant * weight_map

    def calc_cumulative_map(self, energy_map):
        m, n = energy_map.shape
        output = np.copy(energy_map)
        weight = np.copy(self.weight_mask)
        for row in range(1, m):
            for col in range(n):
                output[row, col] = \
                    energy_map[row, col] + np.amin(output[row - 1, max(col - 1, 0): min(col + 2, n - 1)])

                base_index = max(col - 1, 0)
                weight[row, col] = weight[row, col] + weight[row-1, base_index + np.argmin(output[row - 1, base_index: min(col + 2, n - 1)])]
        return output, weight

    def find_seam(self, cumulative_map):
        energy_cumulative_map = cumulative_map[0]
        weight_cumulative_map = cumulative_map[1]

        m, n = energy_cumulative_map.shape
        output = np.zeros((m,), dtype=np.uint32)

        # normalization
        energy_cumulative_map_l2 = np.linalg.norm(energy_cumulative_map)
        weight_cumulative_map_l2 = np.linalg.norm(weight_cumulative_map)
        energy_cumulative_map = energy_cumulative_map / (energy_cumulative_map_l2 if energy_cumulative_map_l2 != 0 else 1)
        weight_cumulative_map = weight_cumulative_map / (weight_cumulative_map_l2 if weight_cumulative_map_l2 != 0 else 1)

        # pick seam
        weight_constant = self.weight_constant if self.weight_after_seam else 0
        output[-1] = np.argmin(energy_cumulative_map[-1] + weight_constant * weight_cumulative_map[-1])

        # find seam in every row
        for row in range(m - 2, -1, -1):
            prv_x = output[row + 1]
            if prv_x == 0:
                output[row] = np.argmin(energy_cumulative_map[row, : 2])
            else:
                output[row] = np.argmin(energy_cumulative_map[row, prv_x - 1: min(prv_x + 2, n - 1)]) + prv_x - 1
        return output

    def delete_seam(self, seam_idx):
        m, n = self.out_image.shape[: 2]
        output = np.zeros((m, n - 1, 3))
        for row in range(m):
            col = seam_idx[row]
            output[row, :, 0] = np.delete(self.out_image[row, :, 0], [col])
            output[row, :, 1] = np.delete(self.out_image[row, :, 1], [col])
            output[row, :, 2] = np.delete(self.out_image[row, :, 2], [col])
        self.out_image = np.copy(output)

    def add_seam(self, seam_idx):
        m, n = self.out_image.shape[: 2]
        output = np.zeros((m, n + 1, 3))
        for row in range(m):
            col = seam_idx[row]
            for ch in range(3):
                if col == 0:
                    p = np.average(self.out_image[row, col: col + 2, ch])
                    output[row, col, ch] = self.out_image[row, col, ch]
                    output[row, col + 1, ch] = p
                    output[row, col + 1:, ch] = self.out_image[row, col:, ch]
                else:
                    p = np.average(self.out_image[row, col - 1: col + 1, ch])
                    output[row, : col, ch] = self.out_image[row, : col, ch]
                    output[row, col, ch] = p
                    output[row, col + 1:, ch] = self.out_image[row, col:, ch]
        self.out_image = np.copy(output)

    def update_seams(self, remaining_seams, current_seam):
        output = []
        for seam in remaining_seams:
            seam[np.where(seam >= current_seam)] += 2
            output.append(seam)
        return output

    def rotate_image(self, image, ccw):
        m, n, ch = image.shape
        output = np.zeros((n, m, ch))
        if ccw:
            image_flip = np.fliplr(image)
            for c in range(ch):
                for row in range(m):
                    output[:, row, c] = image_flip[row, :, c]
        else:
            for c in range(ch):
                for row in range(m):
                    output[:, m - 1 - row, c] = image[row, :, c]
        return output

    def rotate_weight(self, mask, ccw):
        m, n = mask.shape
        output = np.zeros((n, m))
        if ccw > 0:
            image_flip = np.fliplr(mask)
            for row in range(m):
                output[:, row] = image_flip[row, : ]
        else:
            for row in range(m):
                output[:, m - 1 - row] = mask[row, : ]
        return output

    def delete_seam_on_weight(self, seam_idx):
        m, n = self.weight_mask.shape
        output = np.zeros((m, n - 1))
        for row in range(m):
            col = seam_idx[row]
            output[row, : ] = np.delete(self.weight_mask[row, : ], [col])
        self.weight_mask = np.copy(output)

    def add_seam_on_weight(self, seam_idx):
        m, n = self.weight_mask.shape
        output = np.zeros((m, n + 1))
        for row in range(m):
            col = seam_idx[row]
            if col == 0:
                p = np.average(self.weight_mask[row, col: col + 2])
                output[row, col] = self.weight_mask[row, col]
                output[row, col + 1] = p
                output[row, col + 1:] = self.weight_mask[row, col:]
            else:
                p = np.average(self.weight_mask[row, col - 1: col + 1])
                output[row, : col] = self.weight_mask[row, : col]
                output[row, col] = p
                output[row, col + 1:] = self.weight_mask[row, col:]
        self.weight_mask = np.copy(output)

    def save_result(self, filename):
        cv2.imwrite(filename, self.out_image.astype(np.uint8))

    def get_result(self):
        return Image.fromarray(self.out_image.astype(np.uint8))

    def get_weight_mask(self):
        return self.weight_mask


