# Attack By Scale
This is a experiment project aiming to implement attack by scale.

# Main Idea
We assumes there are different in attentions of deep model and human view. Remove the seam, which is important for deep model but indifferent for human from image, to scale the image to attack deepmodel. 

# Method
1. compute the attention of deep model
2. choose the seam which has high attention but less view energy
3. remove them or add others

# Experiment
Experiment on resnet50 on imagenet. The attack turns out failed. 

# Conclusion
The idea lack theory support. We can't interpret how seam removing will affect the output logits. The operation of seam removing can't be exressed with matrixs.

On the other hand, the attention of resnet50 seems uniformly distribute on pixels, which is different from our assumes.